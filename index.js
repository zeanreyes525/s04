/*Demo*/
document.getElementById("btn-1").addEventListener('click', () => {
	alert("Add More!");
});

let paragraph = document.getElementById("paragraph-1");
let paragraph2 = document.getElementById("paragraph-2");

document.getElementById("btn-2").addEventListener('click', () => {
	paragraph.innerHTML = "I can even do this!";
});

document.getElementById("btn-3").addEventListener('click', () => {
	paragraph2.innerHTML = "I purple you!";
	paragraph2.style.color = "purple";
	paragraph2.style.fontSize = "50px";
});

/*Lesson Proper
	Topics
		- Introduction
		- Writing Comments
		- Syntax and Statements
		- Storing values through variables
		- Peeking at Variable's value
		- Data types
		- Functions
JavaScript
	- is a scripting language the enables you to make interactive web pages
	- it is one core technologies of the World Wide Web
	- Was originally intended for web browsers. However, they are now also integrated in web servers through the use of Node.js.

Uses of JavaScript
	- Web app development
	- Browser-based game development
	- Art Creation
	- Mobile applications
*/

// Writing Comment in JavaScript:
// There are two ways of writing comments in JS:

	// - single linecomment - (shortcut) ctrl + /
	// - can only make comments in a single line

	/*
		multi-line comments:
			(shortcut) crtl + shift + /
		comments in JS, much like CSS, and HTML, is not read by the browser. So, these comments are often used to add notes and to add markers to your code.
	*/

console.log("Hello, my name is Thonie. I purple you.");
/*
	JavaScript
		we can see or log message in our console.

		- Consoles are part of our browser which will allow us to see/log messages, data, or information from our programming language.

		- for most browsers, console can be accessed through its developer tools in the console tab.

		In fact, consoles in browser allow us to add some JavaScript expression.

	Statements 
		- are instructions, or expressions we add to our programming language which will then be communicated to our computers
		- Statements in JavaScript commonly ends in semi-colon(;). However, JavaScript has an implemented way of automatically adding semi-colons at the end of our statements. Which, therefore, mean that, unlike other language, JS does NOT require semi-colons.
		- Semicolons in JS are mostly used to mark the end of the statement.

	Syntax 
		- Syntax in programming is a set of rules that describes how statements are properly made/constructed.

		- Lines/block of code must follow a certain rules for it to work. Because remember, you are not merely communicating with another human. In fact, you are communicating with a computer.
*/

console.log("Thonie"); //log this message on the console

/*Variables*/
/*
	In HTML, elements are containers of other element and text
	In JavaScript, variables are containers of data. A given name is used to describe that piece of data.

	Variables also allows us to use or refer to data multiple times.
*/

// num is our variable
// 10 being the value/data
let num = 10;

console.log(6);
console.log(num);

let name = "Jin";
console.log("V");
console.log(name);

/* Creating Variables*/
/*
	To create a varible, there are two steps:
		- Declaration which actually allows to create the variable
		- Initialization which allows to add an initial value to a variable

	Variables in JS are declared with the use of let or const keyword.
*/
let myVariable;
/*
	We can create variables without an initial value. However, when logged into the console, the variable will return "undefined".

	Undefined is a datatype that indicates that variable does exist. However, there is no initial value.
*/
console.log(myVariable);

myVariable = "New Initialized Value";
console.log(myVariable);

/*
	myVariable2 = "Initial Value";
	let myVariable2;
	console.log("myVariable2");
	
	Note: You cannot and should not access a variabl before it's been created/declared

	Can you use or refer to variable that has not been declared or created? 
		No, this will result in an error. Not defined

	Undefined vs Not Defined

	Undefined means the variable has been declared but there is no initial value
		- Undefined is a data type

	Not Defined means that the variable you are trying to refer or access does NOT exist
		- Not defined is an error

	Note: Some error in JS will stop the program from further executing.
*/

let myVariable3 = "Another sample";
console.log(myVariable3);
/*
	Variable must be declared first before they are used, referred to or accessed.

	Using referring to or accessing a variable before it's been declared results an error.
*/

// Let vs Const
/*
	Let
		- With the use of let keyword, we can create variable that can be declared, or initialized and re-assigned

		- We can declare let variables and initialize after
*/
let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);

/*re-assigning let variables*/
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

// Did the value change? Yes. We can re-assign values to the let variables.

const pi = 3.1416;
console.log(pi);

/*
	Const variables are variables with constant data. Therefore we should not re-declare, re-assign, or even declare it.

	Const variables are used for data that we expect or do not want its value to be changed.
*/
// Can you re-assign another value to a const variable? No. An error will occur.
const mvp = "Michael Jordan"
console.log(mvp);
/*mvp = "Lebron James"
console.log(mvp);*/

/*Guides on Variable Names
	1. When naming variables, it is important to create variables that are descriptive and indicative of data it contains.
		let firstName = "RM"; - good variable name
		let pokemon = 25000; - bad >:((((

	2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with a capital letter, because there are several keywords in JS that start in a capital letter.
		let firstName = "JHope";
		let FirstName = "Michael"; 
	3. Do not add to your variable names. Use camelCase for multiple words, or underscore.
		let first name = "Mike"

		camelCase: lastName emailAddress mobileNumber

		under_scores: 
			let product_description = "Lorem ipsum"
*/
let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);

// Declaring multiple variable: use commas to separate each bariable

let brand= "Toyota", model = "Vios", type = "Sedan";
console.log(brand, model, type);

/* 
Data Types
	In most programming languages, data is differentiated by their types. For most programming languages, you have to declare not the variable name but also the type of data you are saving into a variable. However, JS does not require this.

	To create data with a particular data type, some data types require adding with literal.
		string literals = '',"", and most recently: `` (template literals; backtick)
		object literals = {}
		array literals = []
*/

/* 
String:
	- Strings are a series of alphanumeric that create a word, a phrase, a name, or anything related to creating text.

	- String literals such as '' (single quote) or "" (double quote) are used to write/create strings.
*/

let firstName = "Sean Maverick";
let lastName = 'Reyes';

console.log(firstName, lastName);

/*
Mini-Activity:
	1) Create 2 variable named firstName and lastName
	2) Add your first name as string to its appropriate variables
	3) Log your first name and last name variables in the console at the same time.
*/

// Concatination is the process/operation wherein we combine two strings as one. Using the plus (+) sign.
// JS Strings, spaces are also counted as characters.
console.log(firstName + " " + lastName);

let fullName = firstName + " " + lastName;
console.log(fullName);

let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = "De La Salle";
let word6 = "a";
let word7 = "Dasmarinas";
let space = " ";

/*
	Mini-Activity

	Create a variable called sentence.
	Combine the variables to form a single string which would ligibly and understandably create a sentence that you are a student of DLSUD
	Log the sentence variable in the console.

*/

let sentence = fullName + space + word1 + space + word6 + space + word2 + space + word3 + space + word5 + space + word4 + space + word7;
console.log(sentence);

/*
Template literals will allow us to create string with the use of backticks. It also allows us to easily concatinate string without the use of +.

This is also allow to embed or add variable and even expressions in our string with the use of placeholders ${}
*/

/*
	Number (Data type)
		- Integers (whole numbers)
		- Floats (decimals)
	These are our number data which can be used for mathematical operations
*/
let numString1 = "5";
let numString2 = "6";
let num1 = 5;
let num2 = 6;

console.log(numString1 + numString2); //56 string
console.log(num1 + num2)//11 - both argument in the operation are number types

let num3 = 5.5;
let num4 = .5;

console.log(num1+num3);
console.log(num3+num4);

// When the + or addition operator is used on numbers, it will do the proper mathematical operation. However, when used on strings, it will concatenate.

console.log(numString1 + 1);
console.log(num3 + numString2);
// Forced coercion - when one data's type is forced to change to complete an operation
// String + num = concatenation

// parseInt() - this can change the type of numeric string to a proper number
console.log(parseInt(numString1) + num1); //10 - numString1 was parsed into a proper number